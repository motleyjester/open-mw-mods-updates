# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1
from pybuild.info import PV

from common.distutils import Distutils

# Package was renamed, but portmod's renaming feature is a little broken at the moment
PN = "configtool"


class Package(Distutils, Pybuild1):
    NAME = "Portmod OpenMW Config Module"
    DESC = "Sorts openmw.cfg and settings.cfg to match mods installed by portmod."
    LICENSE = "GPL-3"
    KEYWORDS = "openmw"
    HOMEPAGE = f"https://gitlab.com/portmod/{PN}"
    SRC_URI = f"https://gitlab.com/portmod/{PN}/-/archive/{PV}/{PN}-{PV}.tar.gz"
    S = f"{PN}-{PV}/{PN}-{PV}"
    IUSE = "grass map_normal map_specular map_terrain_normal map_terrain_specular"
    RDEPEND = """
        bin/delta-plugin
        dev-python/roundtripini
    """
    DEPEND = "dev-python/setuptools"

    def src_prepare(self):
        super().src_prepare()
        self.SETTINGS = {}
        if "grass" in self.USE:
            self.SETTINGS["Groundcover"] = {
                "enabled": "true",
                "density": 0.5,
                "min chunk size": 0.5,
            }
        map_keys = {
            "map_normal": "auto use object normal maps",
            "map_specular": "auto use object specular maps",
            "map_terrain_normal": "auto use terrain normal maps",
            "map_terrain_specular": "auto use terrain specular maps",
        }
        self.SETTINGS["Shaders"] = {}
        for key in map_keys:
            if key in self.USE:
                self.SETTINGS["Shaders"][map_keys[key]] = "true"
            else:
                self.SETTINGS["Shaders"][map_keys[key]] = "false"
