# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Tamriel Rebuilt"
    DESC = "A project that aims to fill in the mainland of Morrowind"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org/
        http://www.nexusmods.com/morrowind/mods/42145
    """
    OLD_P = "tamriel-rebuilt-20.02"  # Ignore patch version for original file
    LICENSE = OLD_P
    RESTRICT = "mirror"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        >=landmasses/tamriel-data-7.1
        music? ( media-audio/tamriel-rebuilt-music )
        !!land-rocks/on-the-rocks
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        https://drive.google.com/uc?id=1m_601kRMyWBNYFIGW2V-FDwm65ZP-FYM&e=download
        -> {OLD_P}.7z
        https://drive.google.com/uc?id=1n2Nwg1_t0VwT4UQwIDzvgDfuxs2QHJ37&e=download
        -> {P}-patch.7z
    """
    IUSE = "preview travels +music"
    TIER = 2

    INSTALL_DIRS = [
        InstallDir("00 Core", PLUGINS=[File("TR_Mainland.esm")], S=OLD_P),
        InstallDir(
            "01 Faction Integration", PLUGINS=[File("TR_Factions.esp")], S=OLD_P
        ),
        InstallDir(
            "02 Preview Content",
            PLUGINS=[File("TR_Preview.esp")],
            REQUIRED_USE="preview",
            S=OLD_P,
        ),
        InstallDir(
            "03 Travel Network for Core and Vvardenfell",
            PLUGINS=[File("TR_Travels.esp")],
            REQUIRED_USE="travels !preview",
            S=OLD_P,
        ),
        InstallDir(
            "03 Travel Network for Core, Preview, and Vvardenfell",
            PLUGINS=[File("TR_Travels_(Preview_and_Mainland).esp")],
            REQUIRED_USE="preview travels",
            S=OLD_P,
        ),
        # TODO: Requires Abot's Travels
        # InstallDir(
        #     "04 Patch for Abot's Travels mods",
        #     PLUGINS=[
        #         File("TR_OldTravels.esp", ),
        #     ],
        #     REQUIRED_USE="old-travels"
        #     S=OLD_P
        # ),
        InstallDir(
            ".",
            PLUGINS=[File("TR_Mainland_2002_hotfix.esp")],
            S=f"{P}-patch",
        ),
    ]
