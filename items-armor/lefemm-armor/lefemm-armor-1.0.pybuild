# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir, apply_patch


class Package(MW):
    NAME = "LeFemm Armor"
    DESC = "Adds Domina Armor and Gold Armor, designed for female characters"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    IUSE = "minimal"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/lefemmarmor1.1.zip
        https://gitlab.com/bmwinger/umopp/uploads/18a6ecafc4d1e8d30f1be6d982bbc430/lefemmarmor-umopp-3.0.2.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("LeFemmArmor.esp")], S="lefemmarmor1.1")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "lefemmarmor-umopp-3.0.2")
        if "minimal" in self.USE:
            # From UMOPP Nexus Page: Removes the female cuirass edits,
            # as well as edits to Sirollus Saccus.
            apply_patch(os.path.join(path, "LeFemmArmor_compat_plugin.patch"))
        else:
            apply_patch(os.path.join(path, "LeFemmArmor_plugin.patch"))

        apply_patch(os.path.join(path, "A_Domina_Helm_mesh.patch"))
        apply_patch(os.path.join(path, "a_goldArmor_F_H_mesh.patch"))
