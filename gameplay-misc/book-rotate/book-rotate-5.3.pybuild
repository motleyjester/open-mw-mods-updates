# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Book Rotate v5.3"
    DESC = "Allows you to place books vertically and horizontally wherever you please"
    HOMEPAGE = """
        https://web.archive.org/web/20161103115849/http://mw.modhistory.com/download--6953
        https://www.nexusmods.com/morrowind/mods/45564
    """
    LICENSE = "all-rights-reserved attribution"  # TR Patch has an attribution license
    RDEPEND = """
        base/morrowind[bloodmoon?,tribunal?]
        tr? (
            landmasses/tamriel-data
            landmasses/tamriel-rebuilt
        )
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        https://web.archive.org/web/20161103115849/http://mw.modhistory.com/file.php?id=6953 -> {P}.zip
        tr? ( https://gitlab.com/portmod-mirrors/openmw/-/raw/master/Book_Rotate_-_Tamriel_Rebuilt_Patch-45564-1-0.7z )
    """
    IUSE = "bloodmoon tribunal tr"
    REQUIRED_USE = "tr? ( tribunal bloodmoon )"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[
                File("Book Rotate - Bloodmoon v5.3.esp", REQUIRED_USE="bloodmoon"),
                File("Book Rotate.esm", REQUIRED_USE="|| ( tribunal bloodmoon )"),
                File(
                    "Book Rotate - Morrowind v1.1.esp",
                    REQUIRED_USE="!tribunal !bloodmoon",
                ),
                File("Book Rotate - Tribunal v5.3.esp", REQUIRED_USE="tribunal"),
            ],
            S=f"{P}",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Book Rotate - Tamriel Rebuilt Patch.ESP")],
            S="Book_Rotate_-_Tamriel_Rebuilt_Patch-45564-1-0",
            REQUIRED_USE="tr",
        ),
    ]
