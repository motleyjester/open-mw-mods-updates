# a Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import json
import os
import re
import shutil
import sys

from chardet import detect

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Morrowind"
    DESC = "Base morrowind data files by Bethesda"
    HOMEPAGE = "https://elderscrolls.bethesda.net/en/morrowind"
    KEYWORDS = "openmw tes3mp"
    IUSE = "bloodmoon tribunal +plugins"
    TIER = "0"
    # Technically there is a specific EULA, however the user would
    # have accepted it already when installing the files.
    LICENSE = "all-rights-reserved"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("Morrowind.esm"),
                File(
                    "Bloodmoon.esm", REQUIRED_USE="bloodmoon", OVERRIDES="Tribunal.esm"
                ),
                File("Tribunal.esm", REQUIRED_USE="tribunal"),
            ],
            REQUIRED_USE="plugins",
            S="Morrowind",
        ),
        InstallDir(
            ".",
            ARCHIVES=[
                File("Morrowind.bsa"),
                File(
                    "Bloodmoon.bsa", REQUIRED_USE="bloodmoon", OVERRIDES="Tribunal.bsa"
                ),
                File("Tribunal.bsa", REQUIRED_USE="tribunal"),
            ],
            S="Morrowind",
        ),
    ]

    def read_ini(self, path):
        section = None
        self.FALLBACK = {}
        with open(os.path.join(self.FILESDIR, "ini_whitelist.json"), "r") as file:
            whitelist = set(json.load(file))

        with open(path, "rb") as file:
            result = detect(file.read())

        if result["confidence"] < 1.0:
            self.warn(
                f'Detected {result["encoding"]} for Morrowind.ini with confidence of '
                f'only {result["confidence"]}\n'
                "You should double-check that this encoding is correct and that the "
                "fallback entries in opennw.cfg are not garbled."
            )

        with open(path, "r", encoding=result["encoding"]) as config:
            for line in config.readlines():
                if re.match(r"\s*^\[.*\]\s*$", line):
                    section = line.strip().strip("[]")
                    self.FALLBACK[section] = {}
                elif "=" in line:
                    key = line.split("=", 1)[0].strip()
                    value = line.split("=", 1)[1].strip()
                    if f"{section}:{key}" in whitelist:
                        self.FALLBACK[section][key] = value

    # Morrowind must exist on your machine already. All this does is find it
    # and make a copy
    def src_prepare(self):
        destpath = os.path.join(self.WORKDIR, "Morrowind")

        # Detect morrowind installation
        morrowind_bsa = "Morrowind.bsa"
        morrowind_esm = "Morrowind.esm"
        bloodmoon_esm = "Bloodmoon.esm"
        bloodmoon_bsa = "Bloodmoon.bsa"
        tribunal_esm = "Tribunal.esm"
        tribunal_bsa = "Tribunal.bsa"

        roots = []

        # If the user has set MORROWIND_PATH, search that first
        if os.environ.get("MORROWIND_PATH"):
            roots.append(os.environ.get("MORROWIND_PATH"))

        if (
            sys.platform == "win32"
            or sys.platform == "cygwin"
            or os.environ.get("WSL_INTEROP")
        ):
            # Note on WSL, the variables probably aren't set
            if os.environ.get("WSL_INTEROP"):
                PROGRAM_FILES = ["/mnt/c/Program Files", r"/mnt/c/Program Files (x86)"]
            else:
                PROGRAM_FILES = [
                    os.environ["ProgramW6432"],
                    os.environ["ProgramFiles(x86)"],
                ]
            for path in PROGRAM_FILES:
                roots.append(os.path.join(path, "Bethesda Softworks", "Morrowind"))
                roots.append(
                    os.path.join(path, "Steam", "SteamApps", "common", "Morrowind")
                )

            # don't know if registry can be accessed from cygwin python
            # It can't be accessed from WSL
            if sys.platform == "win32":
                from pybuild.winreg import HKEY_LOCAL_MACHINE, read_reg

                roots.append(
                    read_reg(
                        HKEY_LOCAL_MACHINE,
                        r"SOFTWARE\Microsoft\Windows\CurrentVersion"
                        r"\Uninstall\Steam App 22320",
                        "InstallLocation",
                    )
                )
                roots.append(
                    read_reg(
                        HKEY_LOCAL_MACHINE,
                        r"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion"
                        r"\Uninstall\Steam App 22320",
                        "InstallLocation",
                    )
                )
                # FIXME: Need GOG Game identifier
                # roots.append(
                #     read_reg(
                #         HKEY_LOCAL_MACHINE,
                #         r"\SOFTWARE\GOG.com\Games\GOG_ID",
                #         "InstallLocation",
                #     )
                # )

        # If, not elif, so WSL checks both windows and linux paths
        if sys.platform == "linux":
            roots.append("~/.local/share/Steam/steamapps/common/Morrowind")
            roots.append("~/GOG Games/Morrowind")
            roots.append("~/.wine/drive_c/GOG Games/Morrowind")
            roots.append("~/.local/share/openmw/basedata")
            roots.append("~/.wine/drive_c/Program Files/Bethesda Softworks/Morrowind")
            roots.append(
                "~/.wine/drive_c/Program Files/Steam/SteamApps/common/Morrowind"
            )
        elif sys.platform == "darwin":
            roots.append(
                "~/Library/Application Support/Steam/SteamApps/common/Morrowind"
            )
            roots.append(
                "~/Library/Application Support/Steam/SteamApps/common/"
                "The Elder Scrolls III - Morrowind"
            )
            roots.append("~/GOG Games/Morrowind")

        print(
            "Searching for Morrowind installation in "
            # TODO: List comprehension is wordy.
            # Replace with filter(bool, roots) once that's supported by the sandbox
            + ", ".join([root for root in roots if root])
        )
        config_found = False
        for path in roots:
            if not path:
                continue
            for root, dirs, files in os.walk(os.path.expanduser(path)):
                # Ignore portmod directory.
                # We don't want to detect a previously installed version
                if "portmod" in root:
                    continue

                if not config_found:
                    for file in files:
                        if file.lower() == "morrowind.ini":
                            self.read_ini(os.path.join(root, file))
                            config_found = True
                            break
                if (
                    not os.path.exists(destpath)
                    and morrowind_bsa in files
                    and morrowind_esm in files
                ):
                    print(f"Found morrowind data files in directory {root}")

                    if "bloodmoon" in self.USE and not (
                        bloodmoon_esm in files and bloodmoon_bsa in files
                    ):
                        raise Exception(
                            "bloodmoon use flag enabled, "
                            "but bloodmoon could not be found"
                        )
                    if "tribunal" in self.USE and not (
                        tribunal_esm in files and tribunal_bsa in files
                    ):
                        raise Exception(
                            "tribunal use flag enabled, but tribunal could not be found"
                        )

                    try:
                        os.symlink(root, destpath, True)
                    except (OSError, NotImplementedError):
                        shutil.copytree(root, destpath)
                if os.path.exists(destpath) and config_found:
                    break

        if not os.path.exists(destpath):
            raise Exception(
                "Could not find Morrowind data files!\n"
                "Consider setting MORROWIND_PATH in portmod.conf"
            )
        if not config_found:
            raise Exception(
                "Could not find Morrowind.ini!\n"
                "Consider setting MORROWIND_PATH in portmod.conf "
                "and ensure it points to the root of the Morrowind install"
            )

        def pkg_prerm(self):
            if os.path.exists(self.ROOT) and os.path.islink(path):
                os.unlink(self.ROOT)
            super().pkg_prerm(self)
